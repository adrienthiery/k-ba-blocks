<?php
/**
 * Plugin Name:     K Ba Blocks
 * Description:     Blocs pour le site K-ba
 * Version:         0.1.0
 * Author:          OSEDEA
 * License:         GPL-2.0-or-later
 * License URI:     https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:     create-block
 *
 * @package         create-block
 */

/**
 * Registers all block assets so that they can be enqueued through the block editor
 * in the corresponding context.
 *
 * @see https://developer.wordpress.org/block-editor/tutorials/block-tutorial/applying-styles-with-stylesheets/
 */
function create_block_k_ba_blocks_block_init() {
	$dir = dirname( __FILE__ );

	$script_asset_path = "$dir/build/index.asset.php";
	if ( ! file_exists( $script_asset_path ) ) {
		throw new Error(
			'You need to run `npm start` or `npm run build` for the "create-block/k-ba-blocks" block first.'
		);
	}
	$index_js     = 'build/index.js';
	$script_asset = require( $script_asset_path );
	wp_register_script(
		'create-block-k-ba-blocks-block-editor',
		plugins_url( $index_js, __FILE__ ),
		$script_asset['dependencies'],
		$script_asset['version']
	);

	$editor_css = 'build/index.css';
	wp_register_style(
		'create-block-k-ba-blocks-block-editor',
		plugins_url( $editor_css, __FILE__ ),
		array(),
		filemtime( "$dir/$editor_css" )
	);

	$style_css = 'build/style-index.css';
	wp_register_style(
		'create-block-k-ba-blocks-block',
		plugins_url( $style_css, __FILE__ ),
		array(),
		filemtime( "$dir/$style_css" )
	);

	register_block_type( 'create-block/k-ba-blocks', array(
		'editor_script' => 'create-block-k-ba-blocks-block-editor',
		'editor_style'  => 'create-block-k-ba-blocks-block-editor',
		'style'         => 'create-block-k-ba-blocks-block',
	) );
}
add_action( 'init', 'create_block_k_ba_blocks_block_init' );

add_action('wp_enqueue_scripts','kba_faq_js');

function kba_faq_js() {
    wp_enqueue_script( 'kba-faq-js', plugins_url( '/src/faq/toggle.js', __FILE__ ));
}

add_action('wp_enqueue_scripts','code_postal_js', 9999);

function code_postal_js() {
    wp_enqueue_script( 'kba-code-postal-js', plugins_url( '/src/code-postal-check/handleSubmit.js', __FILE__ ));
}

// see https://gotripod.com/insights/enqueue-defer-scripts-wordpress/
if (!(is_admin())) {
    function defer_js($url) {
		if (FALSE !== strpos($url, 'plugins/k-ba-blocks')) {
			return "$url' defer onload='";
		}

		return $url;
    }
    add_filter('clean_url', 'defer_js', 11, 1);
}
