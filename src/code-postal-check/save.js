/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-i18n/
 */
import { __ } from '@wordpress/i18n';

/**
 * The save function defines the way in which the different attributes should
 * be combined into the final markup, which is then serialized by the block
 * editor into `post_content`.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/block-edit-save/#save
 *
 * @return {WPElement} Element to render.
 */
export default function save({ className, attributes }) {
	return (
		<div className={className}>
			<div>
				<h3>{attributes.message || 'Vérifiez si votre code postal est livré par K-ba'}</h3>
				<div>
					<form>
						<input type="text" name="postal_code_check" placeholder={attributes.placeholder || 'ex: 44200'} required="true" />
						<button type="submit">Vérifier</button>
					</form>
				</div>
			</div>
		</div>
	);
}
