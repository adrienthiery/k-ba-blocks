(function() {
	try {
		if (window.location.search !== '') {
			if (window.location.search.replace('?', '').split('=')[0] !== 'postal_code_check') {
				return;
			}
			const postalCode = window.location.search.replace('?', '').split('=')[1].replace(/\ /g, '');

			jQuery('input[name="postal_code_check"]').val(postalCode);

			if (postalCode.length === 5) {
				jQuery.post(
					ajaxSession.ajaxurl,
					{
						action: 'check_postal_code',
						postal_code: postalCode,
						nonce: ajaxSession.nonce,
						get_message: true,
					},
					function (data) {
						// Set success message in #home-delivery
						if (data.includes('Impossible de déterminer le prochain créneau')) {
							window.alert("K-ba arrive prochainement chez vous, inscrivez-vous à la newsletter pour être tenu informé des nouvelles zones couvertes !");
						} else {
							window.alert(data || `Oui, nous livrons ce code postal !`);
						}
					}
				)
					.fail( function( error ) {
						console.error( 'error', error );
						window.alert("Désolé, ce code postal n'est pas valide.");
					} );
			} else {
				window.alert("Désolé, ce code postal n'est pas valide.");
			}
		}
	} catch (e) {
		if (window.location.host !== 'k-ba.fr') {
			console.error(e);
		}
		// Silently error (laziness)
	}
})();
