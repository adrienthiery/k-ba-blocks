/**
 * Registers a new block provided a unique name and an object defining its behavior.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/#registering-a-block
 */
import { registerBlockType } from '@wordpress/blocks';

/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-i18n/
 */
import { __ } from '@wordpress/i18n';

/**
 * Lets webpack process CSS, SASS or SCSS files referenced in JavaScript files.
 * All files containing `style` keyword are bundled together. The code used
 * gets applied both to the front of your site and to the editor.
 *
 * @see https://www.npmjs.com/package/@wordpress/scripts#using-css
 */
import './style.scss';

/**
 * Internal dependencies
 */
import ValeursEdit from './valeurs/edit';
import ValeursSave from './valeurs/save';

/**
 * Every block starts by registering a new block type definition.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/#registering-a-block
 */
registerBlockType( 'k-ba/valeurs', {
	/**
	 * This is the display title for your block, which can be translated with `i18n` functions.
	 * The block inserter will show this name.
	 */
	title: __( 'Valeurs K-ba', 'k-ba' ),

	/**
	 * This is a short description for your block, can be translated with `i18n` functions.
	 * It will be shown in the Block Tab in the Settings Sidebar.
	 */
	description: __(
		'Un bloc pour afficher les valeurs de K-ba.',
		'k-ba'
	),

	/**
	 * Blocks are grouped into categories to help users browse and discover them.
	 * The categories provided by core are `common`, `embed`, `formatting`, `layout` and `widgets`.
	 */
	category: 'widgets',

	/**
	 * An icon property should be specified to make it easier to identify a block.
	 * These can be any of WordPress’ Dashicons, or a custom svg element.
	 */
	icon: 'smiley',

	/**
	 * Optional block extended support features.
	 */
	supports: {
		// Removes support for an HTML mode.
		html: false,
	},

	/**
	 * @see ./edit.js
	 */
	edit: ValeursEdit,

	/**
	 * @see ./save.js
	 */
	save: ValeursSave,

	attributes: {
		image1: {type: 'string'},
		image1Id: {type: 'number'},
		title1: {type: 'string'},
		content1: {type: 'string'},
		image2: {type: 'string'},
		image2Id: {type: 'number'},
		title2: {type: 'string'},
		content2: {type: 'string'},
		image3: {type: 'string'},
		image3Id: {type: 'number'},
		title3: {type: 'string'},
		content3: {type: 'string'},
	},
} );

import PresseEdit from './presse/edit';
import PresseSave from './presse/save';

registerBlockType( 'k-ba/presse', {
	title: __( 'Presse K-ba', 'k-ba' ),
	description: __(
		'Un bloc pour afficher un article de presse parlant de K-ba.',
		'k-ba'
	),
	category: 'widgets',
	icon: 'smiley',
	supports: {
		html: false,
	},
	edit: PresseEdit,
	save: PresseSave,
	attributes: {
		contentType: {type: 'string'},
		articleExtract: {type: 'string'},
		imageUrl: {type: 'string'},
		imageId: {type: 'number'},
		content: {type: 'string'},
		link: {type: 'string'}
	},
} );

import ProducteurEdit from './producteur/edit';
import ProducteurSave from './producteur/save';

registerBlockType( 'k-ba/producteur', {
	title: __( 'Producteur K-ba', 'k-ba' ),
	description: __(
		'Un bloc pour afficher un producteur pour K-ba.',
		'k-ba'
	),
	category: 'widgets',
	icon: 'smiley',
	supports: {
		html: false,
	},
	edit: ProducteurEdit,
	save: ProducteurSave,
	attributes: {
		imageUrl: {type: 'string'},
		imageId: {type: 'number'},
		name: {type: 'string'},
		title: {type: 'string'},
		location: {type: 'string'},
		content: {type: 'string'},
		link: {type: 'string'}
	},
} );

import BannerEdit from './banner/edit';
import BannerSave from './banner/save';

registerBlockType( 'k-ba/banner', {
	title: __( 'Banner K-ba', 'k-ba' ),
	description: __(
		'Un bloc pour afficher un banner pour K-ba.',
		'k-ba'
	),
	category: 'widgets',
	icon: 'smiley',
	supports: {
		html: false,
	},
	edit: BannerEdit,
	save: BannerSave,
	attributes: {
		background: {type: 'string'},
		tagline: {type: 'string'},
		buttonText: {type: 'string'},
		link: {type: 'string'}
	},
} );

import FAQEdit from './faq/edit';
import FAQSave from './faq/save';

registerBlockType( 'k-ba/faq', {
	title: __( 'FAQ K-ba', 'k-ba' ),
	description: __(
		'Un bloc pour afficher un item de faq pour K-ba.',
		'k-ba'
	),
	category: 'widgets',
	icon: 'smiley',
	supports: {
		html: false,
	},
	edit: FAQEdit,
	save: FAQSave,
	attributes: {
		question: {type: 'string'},
		response: {type: 'string'},
	},
} );

import ModeEmploiEdit from './mode-demploi/edit';
import ModeEmploiSave from './mode-demploi/save';

registerBlockType( 'k-ba/mode-demploi', {
	title: __( "Mode d'Emploi K-ba", 'k-ba' ),
	description: __(
		"Un bloc pour afficher le mode d'emploi en 5 étapes.",
		'k-ba'
	),
	category: 'widgets',
	icon: 'smiley',
	supports: {
		html: false,
	},
	edit: ModeEmploiEdit,
	save: ModeEmploiSave,
	attributes: {
		title: {type: 'string'},
		background: {type: 'string'},
		step1MediaUrl: {type: 'string'},
		step1MediaId: {type: 'string'},
		step1Text: {type: 'string'},
		step2MediaUrl: {type: 'string'},
		step2MediaId: {type: 'string'},
		step2Text: {type: 'string'},
		step3MediaUrl: {type: 'string'},
		step3MediaId: {type: 'string'},
		step3Text: {type: 'string'},
		step4MediaUrl: {type: 'string'},
		step4MediaId: {type: 'string'},
		step4Text: {type: 'string'},
		step5MediaUrl: {type: 'string'},
		step5MediaId: {type: 'string'},
		step5Text: {type: 'string'},
	},
	example: {
		background: 'light',
	},
} );

import CodePostalEdit from './code-postal-check/edit';
import CodePostalSave from './code-postal-check/save';

registerBlockType( 'k-ba/code-postal', {
	title: __( "Vérification code postal K-ba", 'k-ba' ),
	description: __(
		"Un bloc pour laisser l'utilisateur valider si son code postal est livré.",
		'k-ba'
	),
	category: 'widgets',
	icon: 'smiley',
	supports: {
		html: false,
	},
	edit: CodePostalEdit,
	save: CodePostalSave,
	attributes: {
		message: {type: 'string'},
		placeholder: {type: 'string'},
	},
	example: {
		message: 'Vérifiez si votre code postal est livré par K-ba',
		placeholder: 'ex: 44200'
	},
} );
