/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-i18n/
 */
import { __ } from '@wordpress/i18n';

/**
 * The save function defines the way in which the different attributes should
 * be combined into the final markup, which is then serialized by the block
 * editor into `post_content`.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/block-edit-save/#save
 *
 * @return {WPElement} Element to render.
 */
export default function save({ className, attributes }) {
	function transformContent(attContent) {
		let content = attContent;
		if (content.includes('- ')) {
			content = content.replace(/-\ /g, '<li>');
			content = content.replace(/\n/g, '</li>');
			content = `<ul>${content}</ul>`;
		}
		return content;
	}

	const content1 = transformContent(attributes.content1);
	const content2 = transformContent(attributes.content2);
	const content3 = transformContent(attributes.content3);

	return (
		<div className={className}>
			<div className="content">
				<div className="column">
					{attributes.image1
						? <img src={attributes.image1} />
						: null
					}
					<div>
						<h3>{attributes.title1}</h3>
						<p dangerouslySetInnerHTML={{ __html: content1 }}></p>
					</div>
				</div>
				<div className="column">
					{attributes.image2
						? <img src={attributes.image2} />
						: null
					}
					<div>
						<h3>{attributes.title2}</h3>
						<p dangerouslySetInnerHTML={{ __html: content2 }}></p>
					</div>
				</div>
				<div className="column">
					{attributes.image3
						? <img src={attributes.image3} />
						: null
					}
					<div>
						<h3>{attributes.title3}</h3>
						<p dangerouslySetInnerHTML={{ __html: content3 }}></p>
					</div>
				</div>
			</div>
		</div>
	);
}
