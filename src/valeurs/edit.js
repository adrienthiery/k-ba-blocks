/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-i18n/
 */
import { __ } from '@wordpress/i18n';

/**
 * Lets webpack process CSS, SASS or SCSS files referenced in JavaScript files.
 * Those files can contain any CSS code that gets applied to the editor.
 *
 * @see https://www.npmjs.com/package/@wordpress/scripts#using-css
 */
import './editor.scss';

import { MediaUpload, RichText } from '@wordpress/block-editor';
import { TextControl, TextareaControl } from '@wordpress/components';

/**
 * The edit function describes the structure of your block in the context of the
 * editor. This represents what the editor will render when the block is used.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/block-edit-save/#edit
 *
 * @param {Object} [props]           Properties passed from the editor.
 * @param {string} [props.className] Class name generated for the block.
 *
 * @return {WPElement} Element to render.
 */
export default function Edit( { className, attributes, setAttributes } ) {
	function createUpdater(name) {
		return function change (event) {
		  var update = {};
		  if (name.includes('image')) {
			update[name] = event.url;
			update[`${name}Id`] = event.id;
		  } else {
			update[name] = event;
		  }

		  setAttributes(update);
		};
	  };

	return (
		<div
			className={ className }
		>
			<div className="column">
				<MediaUpload
					type="image"
					onSelect={createUpdater('image1')}
					value={attributes.image1Id}
					render={(otherProps) => (
						<button onClick={otherProps.open}>
							{attributes.image1 ? <img src={attributes.image1} /> : 'Choisir une image'}
						</button>
					)}
				/>
				<TextControl
					label="Titre"
					value={attributes.title1}
					onChange={createUpdater('title1')}
					placeholder={'Super titre'}
				/>
				<TextareaControl
					label="Contenu"
					value={attributes.content1}
					onChange={createUpdater('content1')}
					placeholder={'Lorem ipsum'}
				/>
			</div>
			<div className="column">
				<MediaUpload
					type="image"
					onSelect={createUpdater('image2')}
					value={attributes.image2Id}
					render={(otherProps) => (
						<button onClick={otherProps.open}>
							{attributes.image2 ? <img src={attributes.image2} /> : 'Choisir une image'}
						</button>
					)}
				/>
				<TextControl
					label="Titre"
					value={attributes.title2}
					onChange={createUpdater('title2')}
					placeholder={'Super titre'}
				/>
				<TextareaControl
					label="Contenu"
					value={attributes.content2}
					onChange={createUpdater('content2')}
					placeholder={'Lorem ipsum'}
				/>
			</div>
			<div className="column">
				<MediaUpload
					type="image"
					onSelect={createUpdater('image3')}
					value={attributes.image3Id}
					render={(otherProps) => (
						<button onClick={otherProps.open}>
							{attributes.image3 ? <img src={attributes.image3} /> : 'Choisir une image'}
						</button>
					)}
				/>
				<TextControl
					label="Titre"
					value={attributes.title3}
					onChange={createUpdater('title3')}
					placeholder={'Super titre'}
				/>
				<TextareaControl
					label="Contenu"
					value={attributes.content3}
					onChange={createUpdater('content3')}
					placeholder={'Lorem ipsum'}
				/>
			</div>
		</div>
	);
}
