/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-i18n/
 */
import { __ } from '@wordpress/i18n';

/**
 * The save function defines the way in which the different attributes should
 * be combined into the final markup, which is then serialized by the block
 * editor into `post_content`.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/block-edit-save/#save
 *
 * @return {WPElement} Element to render.
 */
export default function save({ className, attributes }) {
	return (
		<div className={className}>
			<div className="content ax-w-sm rounded overflow-hidden shadow-lg">
				<div class="orange-container px-6 py-4">
					<span class="type">{attributes.contentType}</span>
					<h4>{attributes.articleExtract}</h4>
					{attributes.imageUrl
						? <img src={attributes.imageUrl} />
						: null
					}
				</div>
				<div class="px-6 py-4">
					<p class="text-gray-700 text-base">{attributes.content}</p>
					{attributes.link ? <a className="text-right" href={attributes.link} target="_blank" rel="noopener noreferrer">Découvrir ></a> : null}
				</div>
			</div>
		</div>
	);
}
