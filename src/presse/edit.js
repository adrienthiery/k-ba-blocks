/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-i18n/
 */
import { __ } from '@wordpress/i18n';

/**
 * Lets webpack process CSS, SASS or SCSS files referenced in JavaScript files.
 * Those files can contain any CSS code that gets applied to the editor.
 *
 * @see https://www.npmjs.com/package/@wordpress/scripts#using-css
 */
import './editor.scss';

import { MediaUpload, RichText } from '@wordpress/block-editor';
import { TextControl, TextareaControl } from '@wordpress/components';

/**
 * The edit function describes the structure of your block in the context of the
 * editor. This represents what the editor will render when the block is used.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/block-edit-save/#edit
 *
 * @param {Object} [props]           Properties passed from the editor.
 * @param {string} [props.className] Class name generated for the block.
 *
 * @return {WPElement} Element to render.
 */
export default function Edit( { className, attributes, setAttributes } ) {
	function createUpdater(name) {
		return function change (event) {
		  var update = {};
		  if (name.includes('image')) {
			update[name] = event.url;
			update[`${name}Id`] = event.id;
		  } else {
			update[name] = event;
		  }

		  setAttributes(update);
		};
	  };

	return (
		<div
			className={ className }
		>
			<TextControl
				label="Type de contenu"
				value={attributes.contentType}
				onChange={createUpdater('contentType')}
				placeholder={'Article'}
			/>
			<TextareaControl
				label="Extrait de l'article"
				value={attributes.articleExtract}
				onChange={createUpdater('articleExtract')}
				placeholder={'Fais-toi livrer tes courses sans emballage en entreprise.'}
			/>
			<MediaUpload
				type="image"
				onSelect={createUpdater('imageUrl')}
				value={attributes.imageId}
				render={(otherProps) => (
					<button onClick={otherProps.open}>
						{attributes.imageUrl ? <img src={attributes.imageUrl} /> : 'Choisir une image'}
					</button>
				)}
			/>
			<TextareaControl
				label="Contenu"
				value={attributes.content}
				onChange={createUpdater('content')}
				placeholder={'Lorem ipsum'}
			/>
			<TextControl
				label="Lien"
				value={attributes.link}
				onChange={createUpdater('link')}
				placeholder={'Lien'}
			/>
		</div>
	);
}
