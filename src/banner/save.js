/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-i18n/
 */
import { __ } from '@wordpress/i18n';

/**
 * The save function defines the way in which the different attributes should
 * be combined into the final markup, which is then serialized by the block
 * editor into `post_content`.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/block-edit-save/#save
 *
 * @return {WPElement} Element to render.
 */
export default function save({ className, attributes }) {
	return (
		<div className={`${className} ${attributes.background} flex items-center`}>
			<h1 class="flex-1">{attributes.tagline}</h1>
			<a href={attributes.link}>
				<button class="button font-bold py-2 px-4 rounded-full">{attributes.buttonText}</button>
			</a>
		</div>
	);
}
