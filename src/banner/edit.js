/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-i18n/
 */
import { __ } from '@wordpress/i18n';

/**
 * Lets webpack process CSS, SASS or SCSS files referenced in JavaScript files.
 * Those files can contain any CSS code that gets applied to the editor.
 *
 * @see https://www.npmjs.com/package/@wordpress/scripts#using-css
 */
import './editor.scss';

import { MediaUpload } from '@wordpress/block-editor';
import { TextControl, TextareaControl, SelectControl } from '@wordpress/components';

/**
 * The edit function describes the structure of your block in the context of the
 * editor. This represents what the editor will render when the block is used.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/block-edit-save/#edit
 *
 * @param {Object} [props]           Properties passed from the editor.
 * @param {string} [props.className] Class name generated for the block.
 *
 * @return {WPElement} Element to render.
 */
export default function Edit( { className, attributes, setAttributes } ) {
	function createUpdater(name) {
		return function change (event) {
		  var update = {};
		  update[name] = event;

		  setAttributes(update);
		};
	  };

	return (
		<div
			className={ `${className} ${attributes.background}` }
		>
			<TextControl
				label="TagLine"
				value={attributes.tagline}
				onChange={createUpdater('tagline')}
				placeholder={'TagLine'}
			/>
			<div>
				<TextControl
					label="Lien"
					value={attributes.link}
					onChange={createUpdater('link')}
					placeholder={'https://...'}
				/>
				<TextControl
					label="Texte bouton"
					value={attributes.buttonText}
					onChange={createUpdater('buttonText')}
					placeholder={'Texte bouton'}
				/>
				<SelectControl
					label="Arrière-plan"
					value={attributes.background}
					options={ [
						{ label: 'Clair', value: 'light', selected: true },
						{ label: 'Foncé', value: 'dark' },
					] }
					onChange={createUpdater('background')}
				/>
			</div>
		</div>
	);
}
