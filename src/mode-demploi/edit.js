/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-i18n/
 */
import { __ } from '@wordpress/i18n';

/**
 * Lets webpack process CSS, SASS or SCSS files referenced in JavaScript files.
 * Those files can contain any CSS code that gets applied to the editor.
 *
 * @see https://www.npmjs.com/package/@wordpress/scripts#using-css
 */
import './editor.scss';

import { MediaUpload } from '@wordpress/block-editor';
import { SelectControl, TextControl, TextareaControl } from '@wordpress/components';

/**
 * The edit function describes the structure of your block in the context of the
 * editor. This represents what the editor will render when the block is used.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/block-edit-save/#edit
 *
 * @param {Object} [props]           Properties passed from the editor.
 * @param {string} [props.className] Class name generated for the block.
 *
 * @return {WPElement} Element to render.
 */
export default function Edit( { className, attributes, setAttributes } ) {
	function createUpdater(name, stepId) {
		return function change (event) {
		  var update = {};

		  if (name.includes('Media')) {
			update[`step${stepId + 1}${name}Url`] = event.url;
			update[`step${stepId + 1}${name}Id`] = event.id;
		  } else {
			update[`step${stepId + 1}${name}`] = event;
		  }
		  setAttributes(update);
		};
	};

	return (
		<div
			className={ `${className} ${attributes.background}` }
		>
			<TextControl
				label="Titre"
				value={attributes.title}
				onChange={(title) => {
					setAttributes({ title });
				}}
				placeholder={'Titre ou pas'}
			/>
			<SelectControl
				label="Arrière-plan"
				value={attributes.background}
				options={ [
					{ label: 'Clair', value: 'light', selected: true },
					{ label: 'Foncé', value: 'dark' },
				] }
				onChange={(background) => {
					setAttributes({ background });
				}}
			/>
			<div className="columns">
				{[{},{},{},{},{}].map((_, index) => (
					<div>
						<MediaUpload
							type="image"
							onSelect={createUpdater('Media', index)}
							value={attributes[`step${index + 1}MediaId`]}
							render={(otherProps) => (
								<button onClick={otherProps.open}>
									{attributes[`step${index + 1}MediaUrl`] ? <img src={attributes[`step${index + 1}MediaUrl`]} /> : 'Choisir une image'}
								</button>
							)}
						/>
						<TextareaControl
							label="Contenu"
							value={attributes[`step${index + 1}Text`]}
							onChange={createUpdater('Text', index)}
							placeholder={'Lorem ipsum'}
						/>
					</div>
				))}
			</div>
		</div>
	);
}
